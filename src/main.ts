/* eslint-disable @typescript-eslint/naming-convention */
import 'normalize.css';
import 'concrete.css';
import m from 'mithril';
import App from './views/app.js';

m.route(document.body, '/', {
	'/': {
		render: () => m(App),
	},
});
