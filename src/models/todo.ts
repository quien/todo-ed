export type Todo = {
	text: string;
	completed: boolean;
	created: Date;
	priority: number;
	deadline: Date;
};

export const loadTodos = (): Todo[] => {
	const todos = localStorage.getItem('todos');

	return todos ? JSON.parse(todos) as Todo[] : [];
};

export const saveTodos = (todos: Todo[]) => {
	localStorage.setItem('todos', JSON.stringify(todos));
};
