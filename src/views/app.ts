import m from 'mithril';
import {type Todo, loadTodos, saveTodos} from '../models/todo.js';
import {bin} from '../icons/bin.js';
import {edit} from '../icons/edit.js';

/**
 * Filtros de la lista de tareas
 */
enum ListFilter {
	All = 'totales',
	Active = 'pendientes',
	Completed = 'completadas',
}

export default class App implements m.ClassComponent {
	readonly todos: Todo[];
	private filter: ListFilter = ListFilter.Active;

	constructor() {
		this.todos = loadTodos();
	}

	/**
	 * Cambia el filtro de la lista
	 */
	toggleFilter() {
		if (this.filter === ListFilter.All) {
			this.filter = ListFilter.Active;
		} else if (this.filter === ListFilter.Active) {
			this.filter = ListFilter.Completed;
		} else {
			this.filter = ListFilter.All;
		}
	}

	/**
	 * 1. Ordena los todos por prioridad
	 * 2. Filtra los todos según el filtro actual
	 * @returns {Todo[]} Todos filtrados y ordenados
	 */
	withFilter() {
		return this.todos
			.sort((a, b) => a.priority - b.priority)
			.filter(todo => {
				if (this.filter === ListFilter.Active) {
					return !todo.completed;
				}

				if (this.filter === ListFilter.Completed) {
					return todo.completed;
				}

				return true;
			});
	}

	view() {
		return m('main', [
			m('section', [
				m('h1', [
					m('span', 'tareas '),
					m('a', {
						role: 'button',
						style: 'text-decoration: underline;',
						onclick: () => {
							// Cambia el filtro
							this.toggleFilter();
						},
					}, this.filter),
				]),

				// Lista de tareas
				// Si está vacía, muestra un mensaje de que no hay tareas
				this.withFilter().length === 0
					? m('p', 'no hay tareas aquí.')
					: m('ul', [
						this.withFilter().map(todo => m('li', [
							m('input[type=checkbox]', {
								checked: todo.completed,
								onchange: () => {
									todo.completed = !todo.completed;
									saveTodos(this.todos);
								},
							}),

							// Cálculo de color de prioridad
							m('span', {
								style: `color: hsl(${todo.priority * 60}, 100%, 50%);`,
							}, ' ⬤ '),
							m('span', ` ${todo.text}`),
							m('span', ` ${todo.deadline.toLocaleString('es-MX')} `),

							m('button', {
								onclick: () => {
									this.todos.splice(this.todos.indexOf(todo), 1);
									saveTodos(this.todos);
								},
							}, m(bin)),

							m('button', {
								onclick() {
									const dialog = document.querySelector<HTMLDialogElement>('#edit')!;
									dialog.showModal();
								},
							}, m(edit)),

							m('dialog', {
								id: 'edit',
							}, m('form', {
								onsubmit: (ev: Event) => {
									ev.preventDefault();

									const form = ev.target as HTMLFormElement;
									const text = form.elements.namedItem('text') as HTMLInputElement;
									const deadline = form.elements.namedItem('deadline') as HTMLInputElement;
									const priority = form.elements.namedItem('priority') as HTMLInputElement;

									todo.text = text.value;
									todo.deadline = new Date(deadline.value);

									console.log(todo.deadline.toLocaleString());

									todo.priority = Number(priority.value);

									saveTodos(this.todos);
									m.redraw();

									// Close the dialog
									const dialog = document.querySelector<HTMLDialogElement>('#edit')!;
									dialog.close();
								},
							}, [
								m('label', [
									m('span', 'tarea'),
									m('input', {
										type: 'text',
										name: 'text',
										value: todo.text,
									}),
								]),

								m('label', [
									m('span', 'prioridad'),
									m('input', {
										type: 'number',
										name: 'priority',
										value: todo.priority,
										min: 0,
										max: 5,
									}),
								]),

								m('label', [
									m('span', 'fecha límite'),
									m('input', {
										type: 'date',
										name: 'deadline',
										value: new Date(todo.deadline).toISOString().split('T')[0],
										min: new Date().toISOString().split('T')[0],
									}),
								]),

								m('button', {
									type: 'submit',
								}, 'guardar'),
							])),
						])),
					]),
			]),

			m('footer', [
				m('form', {
					onsubmit: (ev: Event) => {
						ev.preventDefault();

						const form = ev.target as HTMLFormElement;
						const text = form.elements.namedItem('text') as HTMLInputElement;
						const deadline = form.elements.namedItem('deadline') as HTMLInputElement;
						const priority = form.elements.namedItem('priority') as HTMLInputElement;

						if (text.value) {
							this.todos.push({
								text: text.value,
								completed: false,
								created: new Date(),
								deadline: new Date(deadline.value),
								priority: Number(priority.value),
							});

							text.value = '';

							saveTodos(this.todos);
							m.redraw();
						}
					},
				}, [
					// Priority input
					m('input', {
						type: 'number',
						name: 'priority',
						placeholder: 'prioridad',
						min: 0,
						max: 5,
					}),

					m('input', {
						type: 'text',
						name: 'text',
						placeholder: 'añadir tarea',
						required: true,
					}),

					m('input', {
						type: 'date',
						name: 'deadline',
						required: true,
						min: new Date().toISOString().split('T')[0],
					}),

					m('button', {
						type: 'submit',
					}, 'añadir'),
				]),
			]),
		]);
	}
}
